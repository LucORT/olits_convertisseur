
# Squelette d'une architecture d'application pour des projets d'étudiants.
Auteur: Philippe Alluin p.alluin@phaln.info

Elle est préparée (voir `\composer.jon`) pour utiliser la librairie `Phaln` 
disponible à l'url `https://gitlab.com/phalnprojects/phaln.git`. 
**Attention**, la librairie demande PHP 8.0 minimum, y compris en CLI en raison 
de l'installation via composer.

## Installation:
- Placez-vous dans `www` (ou `htdocs`) et ouvrez un terminal dans ce dossier. 

- Clonez depuis GitLab le projet `skeleton-light` (identification sans doute demandée) 
vers le dossier de votre choix (`MyProj` par exemple):

`git clone https://gitlab.com/phalnprojects/skeleton-light.git MyProj`

- Rendez-vous dans le dossier `MyProj`:

`cd MyProj`

- Installer les librairies (dont `Phaln`) dans le dossier `vendor` à l'aide de
 composer (utilise le fichier `composer.json`):

`composer install`

- Copiez le fichier `appConfig.local.modele.php` puis renommez cette copie en `appConfig.local.php`.
  **Attention**, le fichier `appConfig.local.php` n'est pas poussé vers un dépôt Git, 
  alors que `appConfig.local.modele.php` l'est.

- Editez le fichier `appConfig.local.php` pour réglez les paramètres:
    - Au minimum la définition de `URL_BASE`, qui est l'url d'accès à votre 
application, par exemple `http://localhost/MyProj/` si vous avez suivi la procédure précédente.
    - `$infoBdd` pour connexion à la bdd si vous en utilisez une. 

- Si vous souhaitez utiliser NPM pour installer vos assets (css, js, ...) 
regardez le fichier `package.json` en exemple qui installe `BootStrap 4` et les
 librairies nécessaires et les icônes `fontawesome`. Pour l'utiliser:

`npm install`

## Les dossiers importants:

Le dossier `/config` contient les fichiers de configuration:
- `appConfig.modele.php` est le modèle du fichier de configuration de l'application.
  Copiez ce fichier puis renommez-le en `cappConfig.php` dans lequel vous réglez vos paramètres:
    - `$infoBdd` pour connexion à la bdd. 
    - `$infoMail` pour l'envoit de mails par un serveur SMTP
    - La définition de `URL_BASE`, qui est l'url de base de votre application.
    - La définition d'une constante `DUMP` utile pour activer/désactiver les var_dump, 
à condition d'utiliser la notation:
    `if(DUMP) var_dump($maVar);` ou `dump_var($var, DUMP, 'Un message présentant $var');`

  Les sessions sont démarrées après l'inclusion de `globalConfig.php`. Cela permet 
d'avoir l'autoload défini et pouvoir mettre des objets en session.
  
  **ATTENTION!!** Ce fichier `appConfig.php` est à inclure en début de toutes vos 
pages. **!!ATTENTION**

- `globalConfig.php` contient une configuration générale, l'inclusion de l'autoload 
psr-4 déployé par composer pour l'utilisation des classes, les constantes avec 
les noms des dossiers pour la librairie, etc. Il ne devrait pas être modifié.

Le dossier `/public/pages` contient les pages publiques de l'application et les assets: 
css, images, javascript. Normalement, en mvc, le fichier `/public/pages/index.php` 
est la page principale de l'application... et même potentiellement la seule!

Le dossier `/src` est la dossier de base pour vos développement PHP. Il contient
 les espaces de nom comme `Entities` (attention à la majuscule...).

Le dossier `/tests` est destiné aux tests unitaire réalisé avec `PhpUnit`.

Le dossier `/pocs` contient l'ensemble de vos fichiers de "proof of concept" 
que vous pouvez faire **avant** d'utiliser une classe, une fonction, etc. 
dans une page destinée à la production. Un exemple de fichier poc est fourni.

Le dossier `/logs` contient les fichiers de journalisation des erreurs si elle 
est activée. Si vous attribuez `false` à `DUMP` dans le fichier `\config\appConfig.php`
la journalisation est activée.

Le dossier `/doc` est destiné à recevoir la documentation générée par `PhpDoc` 
ou vos propres fichiers de documentation. Attention à la confidentialité dans ces fichiers!!

Le fichier `/index.php` renvoie vers le fichier `/public/pages/index.php` qui est la
 page d'accueil de l'application.
