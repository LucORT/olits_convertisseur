<?php
declare(strict_types=1);
require_once '../../config/initApp.php';

$disp1 = $_GET["disp1"];
$disp2 = $_GET["disp2"];

$hostname=$infoBdd['host'];
$dbname=$infoBdd['dbname'];
$charset=$infoBdd['charset'];
$user=$infoBdd['user'];
$password=$infoBdd['pass'];

$dsn = "pgsql:host=$hostname;
        dbname=$dbname;
        options='--client_encoding=$charset'";

$conBdd = new PDO($dsn, $user, $password);

echo "A CORRIGER : la requête SQL est exécutée en-dehors d'un repository !!!!<br>";
$req ="SELECT A.id_appareil, nom_appareil, quantite_par_unite, valeur_unite_energie, 
            unite_utilisee, nom_unite_quantite
        FROM APPAREIL A
            JOIN UNITE_ENERGIE UE ON UE.id_unite_energie = A.id_unite_energie
            JOIN QUANTIFIER Q ON Q.id_appareil = A.id_appareil
            JOIN UNITE_QUANTITE UQ ON UQ.id_unite_quantite = Q.id_unite_quantite";

$res = $conBdd->query($req);

$res->setFetchMode(PDO::FETCH_OBJ);
foreach($res as $row){
    if ($disp1==$row->id_appareil){
        $nom1=$row->nom_appareil;
        $quantite_par_unite1=$row->quantite_par_unite;
        $valeur_unite_energie1=$row->valeur_unite_energie;
        $unite_utilisee1=$row->unite_utilisee;
        $nom_unite_quantite1=$row->nom_unite_quantite;
    }
    if ($disp2==$row->id_appareil){
        $nom2=$row->nom_appareil;
        $quantite_par_unite2=$row->quantite_par_unite;
        $valeur_unite_energie2=$row->valeur_unite_energie;
        $unite_utilisee2=$row->unite_utilisee;
        $nom_unite_quantite2=$row->nom_unite_quantite;
    }
}

$calcul = 
    ($unite_utilisee1 * $quantite_par_unite1 * $valeur_unite_energie1) / 
    ($quantite_par_unite2 * $valeur_unite_energie2);

echo "<p>{$nom1} : {$unite_utilisee1} {$nom_unite_quantite1}</p>
        <p>correspond à</p>
        <p>{$nom2} : {$calcul} {$nom_unite_quantite2}</p>";