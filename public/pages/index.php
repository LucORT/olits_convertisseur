<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../../config/initApp.php';

$repo = new Repositories\AppareilRepository();
$lstAppareils = $repo->getAll();

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title><?= TITRE_APP ?></title>
        <?php include_once '../inc/head.php'; ?>
        <script src="../js/jquery-3.6.1.min.js"></script>
    </head>
    <body>
        <?php include_once '../inc/header.php'; ?>
        
        <main>
            <article>
                <header>
                    <h1><?= TITRE_APP ?></h1>
                </header>        
                <form id="conversion" method="get" action="traits/trait.php">
                    <fieldset>
                        <legend>Sélectionner 2 dispositifs :</legend>
                        <p>
                            <p>
                                <label for="disp1">Dispositif 1</label> : 
                                <br/>
                                <select name="disp1" id="disp1" required>
                                    <?php 
                                        foreach ($lstAppareils as $value){
                                            $id = $value -> getid_appareil();
                                            $nom = $value -> getnom_appareil();
                                            echo "<option value ='{$id}'>{$nom}</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="disp2">Dispositif 2</label> : 
                                <br/>
                                <select name="disp2" id="disp2" required>
                                    <?php 
                                        $mapper = new Repositories\AppareilRepository();
                                        $repo = $mapper->getAll();
                                        foreach ($repo as $value){
                                            $id = $value -> getid_appareil();
                                            $nom = $value -> getnom_appareil();
                                            echo "<option value ='{$id}'>{$nom}</option>";
                                        }
                                    ?>
                                </select>
                                <div id = "resultats">
                                </div>
                            </p>
                            <input type="submit" value="C'est parti !" />
                        </p>
                    </fieldset>
                </form>
                <script>
                    $( "#conversion" ).submit(function( event ) {
                        var formData = {
                            disp1: $("#disp1").val(),
                            disp2: $("#disp2").val(),
                    };
                        $.ajax({
                            type: "GET",
                            url: "../traits/trait.php",
                            data: formData,
                        }).done(function (data) {
                            $("#resultats").append(data);
                        });
                        event.preventDefault();
                    });
                </script>

        	</article>
        </main>
        <?php include_once '../inc/footer.php'; ?>
    </body>
</html>