<?php
// LG 20240111 : Activer les exceptions au moins pour l'étape de config (sera désactivé + bas si la config mentionne de ne pas dumper)
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once 'config/initApp.php';
$url = URL_BASE.'public/pages/index.php';
header("location: $url") ;
