<?php
// Fichier de configuration local
// Ce fichier sert de modèle au fichier appConfig.local.php, exclus de Git
// qui contient les éléments de configuration propres à l'ordinateur où s'exécute le code

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', true);               // Affichage des dump et des exceptions si true
define('DUMP_EXCEPTIONS', true);    // Affichage des exceptions si true, même lorsque DUMP est false
define('DUMP_MODE', 'dump');        // Mode d'affichage des dump: 'dump' pour Symfony, 'phaln' pour mode phaln, rien pour var_dump par défaut (la forme dépend de la présence ou non de XDebug)

//  L'url de votre site, sera utile dans les redirections dans vos pages en cas de déplacement du site...
//  En localhost, l'url peut être de la forme 'http://localhost/dev/skeleton_light/' si vous n'avez pas défini de virtual-host
define('URL_BASE', 'http://monSite.net/');

// La durée d'une session (en secondes)
define('SESSION_LENGTH', 1800);

//  Les informations de connexion à la BDD
$infoBdd = array(
    'interface' => 'pdo',   // "pdo" ou "mysqli"
    'type' => 'mysql',      //  mysql ou pgsql
    'host' => '',           // Votre serveur de bdd
    'port' => 3306,         // Par défaut: 3306 pour MySQL, 5432 pour postgreSQL
    'charset' => 'UTF8',    // charset de la bdd
    'dbname' => '',         // Le nom de la bdd
    'user' => '',           // l'utilisateur de la bdd
    'pass' => '',           // le password de l'utilisateur de la bdd
);
