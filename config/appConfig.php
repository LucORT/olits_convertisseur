<?php
// Fichier de configuration global, inclus dans GIT
// Il contient les informations propres à l'application, qui sont identiques quel que soit le poste sur lequel on l'exécute

//  Le titre à donner aux pages du site
define('TITRE_APP', "Convertisseur énergétique - Powered by OLITS");
define('DESCRIPION_APP', "Convertisseur énergétique");
define('VERSION_APP', '0.0.a');
