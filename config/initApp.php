<?php
// Initialisation standard de l'application, en fonction de la configuration définie dans appConfig.php

// Activer les exceptions au moins pour l'étape de config (sera désactivé + bas si la config mentionne de ne pas dumper)
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Lecture de la configuration
require_once 'appConfig.php';
if (!file_exists(__DIR__ . '/appConfig.local.php')) {
    echo "Le fichier " . __DIR__ . "/appConfig.local.php n'existe pas.<br>
        Vous devez le créer en vous servant du modèle proposé dans " . __DIR__ . "/appConfig.local.modele.php." ;
    die ;
}
require_once 'appConfig.local.php';

// Initialisations
session_start();
if(isset($_SESSION['timeout'])) {
    if(time() - $_SESSION['timeout'] > SESSION_LENGTH) {
        // Inactivité de + de 30 minutes (1800 secondes = 30 min)
        // Fermer la session
        session_destroy();
        session_start();
    }
}
$_SESSION['timeout'] = time() ;

// Constantes pour les chemins.
// Les pages étant dans public/pages, il faut remonter 2 fois.
// define('TEMPLATE_PATH', '../../src/Templates');
// define('CLASS_DIR', '../../src/,../../vendor/,../../vendor/phaln/');
$racineDir = dirname(__DIR__) ;
define('TEMPLATE_PATH', $racineDir . '/src/Templates');
define('CLASS_DIR', "$racineDir/src/,$racineDir/vendor/,$racineDir/vendor/phaln/");

// Fichier des fonctions communes
// require_once '../../src/Lib/Functions.php';
require_once "$racineDir/src/Lib/Functions.php";

//  Autoload pour compatibilité Linux (pb des séparateurs d'espace de nom...)
spl_autoload_register(function ($className) {
    $find = false;
    $bases = explode(',', CLASS_DIR);
    $extension = '.php';
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    foreach ($bases as $base) {
        $fullName = $base . $className . $extension;
        if (file_exists($fullName)) {
            require_once($fullName);
            $find = true;
        }
        if($find) break;
    }
    return $find;
});

//  Nécessaire pour éviter le "bug" du json_encode qui travaille différement
//  avec les float (pb de précision) à partir de PHP7.1 
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set('serialize_precision', -1);
}

//  Active ou pas l'affichage de debug et les var_dump
if(!defined('DUMP_EXCEPTIONS'))
    define('DUMP_EXCEPTIONS', TRUE);
if (DUMP_EXCEPTIONS) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
} 
else {
    ini_set('display_errors','Off');  
}

// Connexion à la BDD
if (!isset($infoBdd)) {
    echo "La variable infoBdd n'a pas été initialisée : cela doit être fait dans appConfig.local.php." ;
    die ;  
}
Phaln\BDD::$infoBdd = $infoBdd;

//  FonctionRepositories à utiliser de préférence à var_dump() lorsque XDebug n'est pas configuré sur la configuration
//  Attention, pour voir les affichages, il faut:
//	* soit basculer la constante DUMP à TRUE... 
//	* soit passer TRUE en deuxième paramètre.
//  Vous pouvez passer en 3ème paramètre une chaine de caractères pour commenter le dump.
//  Dans ce cas, vous DEVEZ donner une valeur pour le deuxième paramètre (soit DUMP, soit TRUE).
function dump_var($var, $dump=DUMP, $msg=null)
{
    if($dump) {
		if($msg)
			echo"<p><strong>$msg</strong></p>";
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
    }
}
