INSERT INTO UNITE_ENERGIE (ID_UNITE_ENERGIE, NOM_UNITE_ENERGIE, VALEUR_UNITE_ENERGIE) VALUES
    (-1, 'Calories', 4184),
    (-2, 'Joules', 1), 
    (-3, 'Wattheures', 3600), 
    (-4, 'kiloWattheurs', 3600000); 

INSERT INTO UNITE_QUANTITE (ID_UNITE_QUANTITE, NOM_UNITE_QUANTITE) VALUES 
    (-1, 'Kilogramme'), 
    (-2, 'Heure'), 
	(-3, 'Jour'),
    (-4, 'Degré'), 
    (-5, 'Litre'), 
    (-6, 'm2'), 
    (-7, 'm3'); 

INSERT INTO TYPE_ENERGIE (ID_TYPE_ENERGIE, NOM_TYPE_ENERGIE) VALUES 
    (-1, 'Pétrole'),
    (-2, 'Electrique'), 
    (-3, 'Muscle'); 

INSERT INTO APPAREIL (ID_APPAREIL, NOM_APPAREIL, QUANTITE_PAR_UNITE, ID_UNITE_ENERGIE, ID_TYPE_ENERGIE, ID_APPAREIL_ETRE) VALUES 
    (-1, 'Laptop', 30, -3, -2, -1), 
    (-2, 'Telephone mobile', 10, -3, -2, -1), 
    (-3, 'Serveur', 300, -3, -2, -1), 
    (-4, 'NAS', 10, -3, -2, -1), 
    (-5, 'Voiture', 8285000, -1, -1, -1);

INSERT INTO QUANTIFIER(ID_UNITE_QUANTITE, ID_APPAREIL, UNITE_UTILISEE) VALUES
	(-2, -1, 1),
	(-3, -2, 1),
	(-2, -3, 1),
	(-2, -4, 1),
	(-5, -5, 1);
