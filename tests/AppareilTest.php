<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);
require_once '../config/initApp.php';

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Test appareil</title>
    </head>
    <body>        
       <?php 
            $mapper = new Repositories\AppareilRepository();
            dump_var($mapper, DUMP, 'Le repository:');

            $obj = $mapper->getById(-1);
            dump_var($obj, DUMP, "Appareil 1 :");

            $repo = $mapper->getAll();
            dump_var($repo, DUMP, "Repository :");
            
            $modif = $mapper->getById(-1);
            $modif -> setcommentaire_appareil('essai de commentaire 2');
            $mapper-> save($modif);

            $enr = $mapper->getById(-1);
            dump_var($enr, DUMP, "Appareil 1 modifié :");

            $creat = new Entities\Appareil();
            $creat -> setid_appareil(-5);
            $creat -> setnom_appareil('Vélo');
            $creat -> setquantite_par_unite(1);
            $creat -> setcommentaire_appareil('Vide');
            $creat -> setid_unite_energie(-1);
            $creat -> setid_type_energie(-3);
            $creat -> setid_appareil_etre(-1);
            $mapper-> save($creat);

            $creat_enr = $mapper->getById(-5);
            dump_var($creat_enr, DUMP, "Nouvel Appareil :");

            
            $suppr = $mapper->getById(-5);
            $mapper->deleteEntity($suppr);

            $repo = $mapper->getAll();
            dump_var($repo, DUMP, "Repository :");



            
       ?> 
    </body>
</html>