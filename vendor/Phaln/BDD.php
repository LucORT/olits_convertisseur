<?php

namespace Phaln;

use Phaln\Exceptions\BDDException;
use \PDO;

/**
 * Classe encapsulant sous forme de singleton une connexion MySQLi ou PDO à la base de données.
 * Elle ne peut pas être dérivée.
 */
final class BDD {

	private static $_bdd = null; //  L'instance singleton
	private $connexion;  //  La connexion MySQLi ou PDO
	public static $infoBdd; //  Les informations de la base de données. Voir les fichiers de config

	/**
	 * 	Constructeur privé, singleton oblige...
	 */

	private function __construct() {
		$db = null;

		try {
			dump_var(BDD::$infoBdd, DUMP, 'infoBdd: ');
			$mydriver = (isset(BDD::$infoBdd['type'])) ? strtolower(BDD::$infoBdd['type']) : 'mysql';
			$myport = (isset(BDD::$infoBdd['port'])) ? BDD::$infoBdd['port'] : (($mydriver === 'mysql') ? 3306 : 5432);
			$mycharset = (isset(BDD::$infoBdd['charset'])) ? BDD::$infoBdd['charset'] : 'UTF8';
			$hostname = (isset(BDD::$infoBdd['host'])) ? BDD::$infoBdd['host'] : "localhost";
			$mydbname = (isset(BDD::$infoBdd['dbname'])) ? BDD::$infoBdd['dbname'] : null;
			$myusername = (isset(BDD::$infoBdd['user'])) ? BDD::$infoBdd['user'] : null;
			$mypassword = (isset(BDD::$infoBdd['pass'])) ? BDD::$infoBdd['pass'] : null;

			if ($hostname == null || $mydbname == null || $myusername == null || $mypassword == null) {
				throw new BDDException('Il manque des informations essentielles à BDD::$infoBdd[].');
			}
			$connect_error = '';
			try {
				//  Composition du DSN
				$dsn = "$mydriver:dbname=$mydbname;host=$hostname;port=$myport";
				dump_var($dsn, DUMP, 'DSN: ');
				switch ($mydriver) {
					case 'mysql':
						$db = new PDO($dsn, $myusername, $mypassword, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $mycharset"));
// LG 20200204 old				$db = new BDDPDO($dsn, $myusername, $mypassword, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $mycharset"));
						break;
					case 'pgsql':
						$dsn .= ';options=\'--client_encoding=' . $mycharset . '\'';
// throw new BDDException('toto');
						$db = new PDO($dsn, $myusername, $mypassword);
// LG 20200204 old				$db = new BDDPDO($dsn, $myusername, $mypassword);
						break;
				}
				if ($db) {
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$this->connexion = $db;
				} else {
// LG 20200828 old					throw new Exceptions\BddException('Instanciation Bdd impossible');
					throw new BDDException('Instanciation Bdd impossible');
				}
			} catch (PDOException $e) {
				$connect_error = $e->getMessage();
				if (DUMP)
					echo 'PDOException dans BDD: ' . $connect_error . '<br/>';
// LG 20200828 old				$ex = new Exceptions\BddException('Instanciation Bdd impossible <br/>' . $connect_error . '<br/>' . $e->getTraceAsString());
				$ex = new BDDException('Instanciation Bdd impossible <br/>' . $connect_error . '<br/>' . $e->getTraceAsString());
				throw $ex;
			}
			dump_var($this->connexion, DUMP, 'Connexion BDD Ok: ');
		}// fin try
		catch (\Throwable $ex) {
// LG 20200828 début : quand PDO lance une exception ci-dessus, relancer ici une exception cause une fatal error sans explications
//// 			$myEx = new Exceptions\BddException($ex->getMessage() . ' ' . $ex->getFile());
//			$myEx = new BDDException($ex->getMessage() . ' ' . $ex->getFile());
//			throw $myEx;
			echo 'Échec de la connexion à la base de données : ' . $ex->getMessage();
			exit;
// LG 20200828 fin
		}
	}

	/**
	 * L'accès à l'instance unique.
	 * @return BDD l'instance singleton.
	 */
	public static function get_bdd() {
		// Si l'instance n'existe pas, on exécute le constructeur pour la créer
		if (is_null(self::$_bdd)) {
			self::$_bdd = new self;
		}

		//  On retourne l'instance existante, qui vient d'être créée ou qui existait déjà.
		return self::$_bdd;
	}

	/**
	 *  __clone vide pour s'assurer de ne pas pouvoir créer de copie du singleton
	 */
	private function __clone() {
		
	}

	/**
	 * Lecteur de la connexion à la bdd (PDO)
	 * @return PDO l'objet accèdant à la base de données.
	 */
	public function get_connexion() {
		return $this->connexion;
	}

	/**
	 * Lecteur statique de la connexion à la bdd (PDO)
	 * @return PDO l'objet accèdant à la base de données.
	 */
	public static function getConnexion() {
		return self::get_bdd()->connexion;
	}

}
