<?php
namespace Phaln;

/**
 * Classe abstraite AbstractEntity encapsulant les accesseurs en lecture et écriture
 * par défaut aux attributs d'une classe.
 * N'a de sens que pour les classes de la couche modèle qui ne contiennent
 * que des attributs et leurs accesseurs.
 *
 * @author Philippe
 */
abstract class AbstractEntity implements \JsonSerializable {

    /**
     * Constructeur, se contente d'appeller la méthode d'hydratation.
     * @param array $datas le tableau associatif des attributs à affecter
     */
    public function __construct(array $datas = NULL) {
	$this->hydrate($datas);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas = NULL) {
		//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
		$attrib = get_class_vars(get_class($this));

		// Appelle le mutateur des attributs existant dans le tableau $datas
		foreach ($attrib as $key => $val) {
	/*
			// Si, dans vos habitudes, les champs de la bdd sont en minuscules, décommentez la ligne suivante.
			// $key = strtolower($key);
			// Si, dans vos habitudes, les champs de la bdd sont en majuscules, décommentez la ligne suivante.
			//$key = strtoupper($key);
			//  Perso, je met tout en notation camelCase... même dans la BDD.
			if (isset($datas[$key])) {
				$mutateur = 'set' . $key;
				$this->$mutateur($datas[$key]);
			}
	*/
			// Appeller le mutateur correspondant à la clé étudiée, selon sa casse
			if (isset($datas[$key])) {
				$mutateur = 'set' . $key;
				$this->$mutateur($datas[$key]);
			} else if(isset($datas[strtolower($key)])) {
				$key = strtolower($key);
				$mutateur = 'set' . $key;
				$this->$mutateur($datas[$key]);
			} else if(isset($datas[strtoupper($key)])) {
				$key = strtoupper($key);
				$mutateur = 'set' . $key;
				$this->$mutateur($datas[$key]);
			} else {
// echo $key . "<br>" ;				
			}
		}

		return $this;
    }

    /**
     * Mutateur simple d'accès en écriture à un attribut.
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut à modifier
     * @param type $valeur la nouvelle valeur de l'attribut
     * @return \AbstractEntity l'objet courant pour faciliter le chaînnage des set.
     */
    protected function set($attribut, $valeur) {
// echo $attribut . "->" . $valeur . "<br>";
		$this->$attribut = $valeur;
	return $this;
    }

    /**
     * Lecteur simple d'accés en lecture à un attribut
     * Peut être surchargé.
     * @param type $attribut le nom de l'attribut
     * @return type la valeur de l'attribut
     */
    protected function get($attribut) {
	return $this->$attribut;
    }

    /**
     * Méthode "magique" interceptant l'appel de méthode et cherchant a exécuter
     * les accesseurs.
     * @param type $methode nom complet de la méthode
     * @param type $attribValeur la valeur passée en paramètre à la méthode
     * @return type le résultat de l'exécution
     */
    public function __call($methode, $attribValeur) {
	$attribName = NULL;

	//  Le préfixe, normalement get ou set
	$prefix = substr($methode, 0, 3);

	//  Le suffixe, normalement un nom d'attribut
	$suffix = substr($methode, 3);

	//  Le nombre de valeur à affecter à l'attribut, 
	//  normalement 0 pour un get et 1 pour un set
	$cattrs = count($attribValeur);

	//  Vérification de l'existence de l'attribut correspondant au suffixe.
	//  La première lettre du suffixe peut être en majuscule ou non.
	if (property_exists($this, $suffix))
	    $attribName = $suffix;
	else if (property_exists($this, lcfirst($suffix)))
	    $attribName = lcfirst($suffix);

// LG 20200208 début
	if ($attribName == NULL) {
		// Tenter de trouver la propriété même avec une autre casse
		$laProps = $this->__toArray();
		foreach($laProps as $key => $values) {
			if (strtolower($key) == strtolower($suffix)) {
				// Cette propriété est bien celle qu'on cherche
				$attribName = $key ;
				break;
			}
		}
	}
// LG 20200208 fin
	
	//  Il existe bien un attribut correspondant au suffixe
	if ($attribName != NULL) {
	    if ($prefix == 'set' && $cattrs == 1)
			return $this->set($attribName, $attribValeur[0]);
	    if ($prefix == 'get' && $cattrs == 0)
			return $this->get($attribName);
	} else
	    throw new Exceptions\EntityException("La méthode $methode n'existe pas...");
    }

    /**
     * La classe implémente l'interface JsonSerializable ce qui permet 
     * aux classes concrête de pouvoir être sérialisée en JSON avec json_encode()
     * @return Array le tableau pour conversion en json
     */
    public function jsonSerialize() {
	$array = array();

	//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));

	// Associe la clé du nom de l'attribut à la valeur de cet attribut
	foreach ($attrib as $key => $val) {
	    $array[$key] = $this->get($key);
	}
	return $array;
    }

    /**
     * Pour affichage simple, style csv, d'un objet
     * @return string une string composée des valeurs des attributs séparés par un ';'
     */
    public function __toString() {
	$string = "";

	//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));

	// Ajoute la valeur de chaque attribut à la $string
	foreach ($attrib as $key => $val) {
	    $string .= $this->get($key) . ';';
	}

	return $string;
    }

    /**
     * Transforme un objet en un tableau associatif.
     * @return array tableau associatif des attributs => valeurs
     */
    public function __toArray() {
	return $this->jsonSerialize();
    }

}
