<?php

namespace Phaln;

/**
 * AbstractValidator permet de définir des règles de validation basiques pour les entités
 *
 * @author phaln
 */
abstract class AbstractValidator {

    /**
     * Contient les filtres PHP à appliquer aux attributs des entités.
     * Voir https://www.php.net/manual/fr/filter.filters.php
     * Tableau associatif: les clés sont les noms des attributs et les valeurs les filtres
     * @var array
     */
    protected $attributFilters = array();
    
    /**
     * Contient les tailles des champs/attributs (dans le cas de string) conforment avec la bdd
     * @var array 
     */
    protected $attributSizes = array();
    
    /**
     * Contient les attributs qui ne peuvent pas être null.
     * @var array 
     */
    protected $attributRequired = array();

    /**
     * Affecte le tableau des tailles
     * @param array $sizes
     * @return $this
     */
    public function setAttributSizes(array $sizes)
    {
	$this->attributSizes = $sizes;
	return $this;
    }
    
    /**
     * Retourne le tableau des tailles
     * @return type
     */
    public function getAttributSizes()
    {
	return $this->attributSizes;
    }
    
    /**
     * Affecte le tableau des filtres
     * @param array $filters
     * @return $this
     */
    public function setAttributFilters(array $filters)
    {
	$this->attributFilters = $filters;
	return $this;
    }
    
    /**
     * Retourne le tableau des filtres
     * @return type
     */
    public function getAttributFilters()
    {
	return $this->attributFilters;
    }
    
    /**
     * Affecte le tableau des attributs non-nullable
     * @param array $filters
     * @return $this
     */
    public function setAttributRequired(array $required)
    {
	$this->attributRequired = $required;
	return $this;
    }
    
    /**
     * Retourne le tableau des attributs non-nullable
     * @return type
     */
    public function getAttributRequired()
    {
	return $this->attributRequired;
    }
    
    /**
     * Ajoute un élément au tableau des filtres
     * @param type $attributName
     * @param type $filterName
     * @return $this
     */
    public function addAttributFilters($attributName, $filterName)
    {
	$this->attributFilters[$attributName] = $filterName;
	return $this;
    }    
    
    /**
     * Ajoute un élément au tableau des tailles
     * @param type $attributName
     * @param type $size
     * @return $this
     */
    public function addAttributSize($attributName, $size)
    {
	$this->attributSizes[$attributName] = $size;
	return $this;
    }    
    
    
    /**
     * Ajoute un élément au tableau des attributs non-nullable
     * @param type $attributName
     * @param type $required
     * @return $this
     */
    public function addAttributRequired($attributName, $required)
    {
	$this->attributSizes[$attributName] = $required;
	return $this;
    }    
    /**
     * Constructeur.
     * @param array $validators Tableau associatif avec les clés 'Filters' et 'Sizes' qui sont les tableaux à affecter aux attributs
     */
    public function __construct(array $validators = NULL) 
    {
	if(isset($validators['Filters']))
	{
	    $this->setAttributFilters($validators['Filters']);
	}
	if(isset($validators['Sizes']))
	{
	    $this->setAttributSizes($validators['Sizes']);
	}
	if(isset($validators['Required']))
	{
	    $this->setAttributRequired($validators['Required']);
	}
    }
    
    /**
     * Recoit une entité et valide ses attributs avec les filtres et tailles dispo.
     * @param \Phaln\AbstractEntity $entity Entité non validée
     * @return \Phaln\AbstractEntity Entité validée
     */
    public function validateEntity(AbstractEntity $entity) {
	//  Récupère la liste des attributs de l'entité
	$attributs = $entity->__toArray();

	// Appelle le mutateur des attributs existant dans le tableau $datas
	foreach ($attributs as $name => $val) {
	    if (isset($this->attributFilters[$name])) 
	    {
		//  L'attribut est du bon type
		$tmp = filter_var($val, $this->attributFilters[$name], FILTER_NULL_ON_FAILURE);
		$tmp = ($tmp) ? $tmp : null;
		
		//  S'il est null et obligatoire, alors exception
		if(isset($this->attributRequired[$name]) && 'REQUIRED' === strtoupper($this->attributRequired[$name]) && null == $tmp)
		    throw new \Phaln\Exceptions\ValidatorException("Champ '$name' obligatoire.");
		
		// Tronque à la taille max.
		if (isset($this->attributSizes[$name]) && null != $tmp) {
		    $tmp = ($temp = substr($tmp, 0, $this->attributSizes[$name])) ? $temp : $tmp;
		}
		
		//  Affecte l'attribut
		$mutateur = 'set' . $name;
		$entity->$mutateur($tmp);
	    }
	}

	return $entity;
    }

    abstract public function createValidEntity(array $datas);
}
