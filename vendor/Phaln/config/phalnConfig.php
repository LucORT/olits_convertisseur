<?php
//  Nécessaire pour éviter le "bug" du json_encode qui travaille différement
//  avec les float (pb de précision) à partir de PHP7.1 
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set('serialize_precision', -1);
}


// Permet de définir la constante DUMP si elle n'est pas définie dans la config de l'application
if(!defined('DUMP'))
    define('DUMP', FALSE);

// Si la constante LOG_DIR n'est pas définie, elle est positionnée à dossier_racine/log
if(!defined('LOG_DIR'))
{
    define('LOG_DIR', dirname(dirname(dirname(dirname(dirname(__FILE__))))).DIRECTORY_SEPARATOR.'log'.DIRECTORY_SEPARATOR);
}

// Si la constante URL_BASE n'est pas définie, elle est positionnée à dossier_racine/log
if(!defined('URL_BASE'))
{
    define('URL_BASE', 'http://demophaln.phaln.info');
}

//  Active ou pas l'affichage de debug et les var_dump
if (DUMP) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
} 
else {
    ini_set('display_errors','Off');  
}


//  Fonction à utiliser de préférence à var_dump() lorsque XDebug n'est pas configuré sur la configuration
//  Attention, pour voir les affichages, il faut:
//	* soit basculer la constante DUMP à TRUE dans votre application... 
//	* soit passer TRUE en deuxième paramètre.
//  Vous pouvez passer en 3ème paramètre une chaîne de caractères pour commenter le dump.
//  Dans ce cas, vous DEVEZ donner une valeur pour le deuxième paramètre (soit DUMP, soit TRUE).
function dump_var($var, $dump=DUMP, $msg=null)
{
    if($dump) {
	if($msg)
	    echo"<p><strong>$msg</strong></p>";
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
    }
}