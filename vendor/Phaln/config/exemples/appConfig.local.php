<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

//  Lancement des sessions
session_start();

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', FALSE);


//  L'url de votre site, sera utile dans les pages en cas de déplacement du site...
define('URL_BASE', "http://host/myWebSite/");


//  Vos informations de connexion à la BDD
$infoBdd = array(
		'interface' => 'pdo',	    // "pdo" ou "mysqli"
		'type'   => 'mysql',	    //  mysql ou pgsql
		'host'   => '',
		'port'   =>  3306,	    // Par défaut: 5432 pour postgreSQL, 3306 pour MySQL
		'charset' => 'UTF8',
    		'dbname' => '',
		'user'   => '',
		'pass'   => '',
	);

require_once 'globalConfig.php';
