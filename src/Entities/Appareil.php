<?php
namespace Entities;

/**
 * Description of Appareil
 *
 * @author phaln
 */
class Appareil extends \Phaln\AbstractEntity{
    protected ?int $id_appareil=NULL;
    protected string $nom_appareil='';
    protected string $commentaire_appareil='';
    protected int $quantite_par_unite=0;
    protected ?int $id_unite_energie=null;
    protected ?int $id_type_energie=null;
    protected ?int $id_appareil_etre=null;

    // function getid_appareil(){
    //     return $this-> id_appareil;
    // }

    // function getnom_appareil(){
    //     return $this-> nom_appareil;
    // }

    // function getcommentaire_appareil(){
    //     return $this-> commentaire_appareil;
    // }

    // function getquantite_par_unite(){
    //     return $this-> quantite_par_unite;
    // }

    // function getid_unite_energie(){
    //     return $this-> id_unite_energie;
    // }

    // function getid_type_energie(){
    //     return $this-> id_type_energie;
    // }

    // function getid_appareil_etre(){
    //     return $this-> id_appareil_etre;
    // }

    // function setid_appareil($pi_id_appareil){
    //     $this-> id_appareil = $pi_id_appareil;
    // }

    // function setnom_appareil($pi_nom_appareil){
    //     $this-> nom_appareil = $pi_nom_appareil;
    // }

    // function setcommentaire_appareil($pi_commentaire_appareil){
    //     $this-> commentaire_appareil = $pi_commentaire_appareil;
    // }

    // function setquantite_par_unite($pi_quantite_par_unite){
    //     $this-> quantite_par_unite = $pi_quantite_par_unite;
    // }

    // function setid_unite_energie($pi_id_unite_energie){
    //     $this-> id_unite_energie = $pi_id_unite_energie;
    // }
    // function setid_type_energie($pi_id_type_energie){
    //     $this-> id_type_energie = $pi_id_type_energie;
    // }
    // function setid_appareil_etre($pi_id_appareil_etre){
    //     $this-> id_appareil_etre = $pi_id_appareil_etre;
    // }

}
