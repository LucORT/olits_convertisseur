<?php

namespace Repositories;

use Phaln\AbstractRepository;

/**
 * Description of AppareilRepository
 *
 * @author phaln
 */
class AppareilRepository extends AbstractRepository {
    public function __construct() {
	parent::__construct(); //  Appel du constructeur de Phaln\AbstractRepository

	$this->table = 'appareil'; // Nom de la table à mapper avec l’entité !! ATTENTION aux majuscules sur Linux... 
	$this->classMapped = 'Entities\Appareil'; // Nom de l’entité à mapper avec la table
	$this->idFieldName = 'id_appareil'; // nom (ou tableau de noms) du (des) champ(s) clé(s) primaire(s)
        $this->isPkAutoIncremented = true; // L'identifiant est autoincrémentée.
    }
}

